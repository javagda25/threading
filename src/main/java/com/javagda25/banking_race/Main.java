package com.javagda25.banking_race;

public class Main {
//
//    1. Tworzymy klasy:
//    - KontoBankowe, które posiada pole:
//          - double stanKonta;
//    oraz metody:
//          - przelewPrzychodzacy(double kwota)
//          - przelewWychodzacy(double kwota)
//
//    - Bank, która posiada pola:
//          - ExecutorService serwer = Executors.newSingleThreadExecutor();
//          - KontoBankowe konto = new KontoBankowe();
//    oraz metody:
//          - dodajZleceniePrzelewu(double kwota, KierunekPrzelewu kierunek);
//
//          Powyższa metoda ma stworzyć nowy obiekt klasy "Zlecenie". Klasa Zlecenie
//          Implementuje interfejs Runnable. Klasa zlecenie posiada 3 pola:
//              - KontoBankowe kontoNaKtorePrzelacPieniadze;
//              - double kwota;
//              - KierunekPrzelewu kierunek
//          Wszystkie parametry przyjmuje w konstruktorze. Klasa posiada pełny konstruktor.
//
//      Zlecenie przelwu po tym jak zostanie stworzone ma być przekazywane do serwera
//      do wykonania. Zlecenie ma metodę "run" którą implementuje z interfejsu Runnable.
//      Zaimplementuj tą metodę następująco:
//          -metoda do konta bankowego które posiada jako pole dodaje lub odejmuje kwotę
//          w zależności od tego jaki jest kierunek przelewu. Jeśli kierunek jest Przychodzący,
//          to dodaje do stanu konta, jeśli jest wychodzący, to odejmuje.
//
//    w klasie bank zaimplementuj ostatnią, dodatkową metodę: "sprawdzStanKonta" która wypisuje
//    ile pieniędzy zostało na koncie.

    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.sprawdzStan();

        for (int i = 0; i < 1000; i++) {
            if(i%2==0){
                bank.dodajZleceniePrzelewu(10, KierunekPrzelewu.PRZYCHODZACY);
            }else{
                bank.dodajZleceniePrzelewu(10, KierunekPrzelewu.WYCHODZACY);
            }
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        bank.sprawdzStan();
    }
}
