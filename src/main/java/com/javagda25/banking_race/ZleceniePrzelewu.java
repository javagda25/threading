package com.javagda25.banking_race;

public class ZleceniePrzelewu implements Runnable {
    private KontoBankowe kontoBankowe;
    private double kwota;
    private KierunekPrzelewu kierunekPrzelewu;

    public ZleceniePrzelewu(KontoBankowe kontoBankowe, double kwota, KierunekPrzelewu kierunekPrzelewu) {
        this.kontoBankowe = kontoBankowe;
        this.kwota = kwota;
        this.kierunekPrzelewu = kierunekPrzelewu;
    }

    @Override
    public void run() {
        if(kierunekPrzelewu == KierunekPrzelewu.WYCHODZACY){
            kontoBankowe.przelewWychodzacy(kwota);
        }else{
            kontoBankowe.przelewPrzychodzacy(kwota);
        }
    }
}
