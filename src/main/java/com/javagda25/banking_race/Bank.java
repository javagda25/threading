package com.javagda25.banking_race;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Bank {
    private KontoBankowe kontoBankowe = new KontoBankowe();
    private ExecutorService serwer = Executors.newFixedThreadPool(5);

    public void dodajZleceniePrzelewu(double kwota, KierunekPrzelewu kierunek){
        serwer.submit(new ZleceniePrzelewu(kontoBankowe, kwota, kierunek));
    }
    public void sprawdzStan(){
        System.out.println(kontoBankowe.getStan());
    }
}
