package com.javagda25.biegacze;

public class Main {
    public static void main(String[] args) {
        Biegacz[] biegaczs = new Biegacz[5];
        biegaczs[0] = new Biegacz("Marian");
        biegaczs[1] = new Biegacz("Lucjan");
        biegaczs[2] = new Biegacz("Łukasz");
        biegaczs[3] = new Biegacz("Rafau");
        biegaczs[4] = new Biegacz("Eutychjusz");

        Thread[] threads = new Thread[5];
        for (int i = 0; i < 5; i++) {
            threads[i] = new Thread(biegaczs[i]);
        }

        for (int i = 0; i < 5; i++) {
            threads[i].start();
        }

        boolean czyKtosSkoczyl = false;
        while (!czyKtosSkoczyl) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print("Biegacze przebiegli: ");
            for (int i = 0; i < 5; i++) {
                System.out.print(biegaczs[i].getDystans() + " ");
                if (biegaczs[i].getDystans() >= 100) {
                    czyKtosSkoczyl = true;
                }
            }
            System.out.println();
        }

        for (int i = 0; i < 5; i++) {
            threads[i].interrupt();
//            biegaczs[i].przestanBiegac();
        }
        for (int i = 0; i < 5; i++) {
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
