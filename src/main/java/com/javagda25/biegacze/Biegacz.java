package com.javagda25.biegacze;

import java.util.Random;

public class Biegacz implements Runnable {
    private String imie;
    private long dystans = 0;
    private boolean czyBiegne;

    public Biegacz(String imie) {
        this.imie = imie;
        this.czyBiegne = true;
    }

    public void run() {
        while (czyBiegne) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                break;
            }
            int dystansKtoryPrzebieglemWTymObiegu = new Random().nextInt(10) + 1;

            System.out.println("Przebiegłem: " + dystansKtoryPrzebieglemWTymObiegu);

            dystans += dystansKtoryPrzebieglemWTymObiegu;
        }
    }

    public void przestanBiegac(){
        czyBiegne = false;
    }

    public String getImie() {
        return imie;
    }

    public long getDystans() {
        return dystans;
    }
}
