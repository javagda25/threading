package com.javagda25.przyklad;

public class Main {
    public static void main(String[] args) {
        Zadanie[] zadania = new Zadanie[5];
        for (int i = 0; i < zadania.length; i++) {
            zadania[i] = new Zadanie("Marian " + i);
        }

        Thread[] tablicaThreadow = new Thread[5];
        for (int i = 0; i < tablicaThreadow.length; i++) {
            tablicaThreadow[i] = new Thread(zadania[i]);
        }

        for (Thread thread : tablicaThreadow) {
            thread.start();
        }

        for (Thread thread : tablicaThreadow) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Koniec");
    }
}
