package com.javagda25.executors;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class PrzykladZajetoscProcka {
    public static void main(String[] args) {

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        executorService.submit(new Runnable() {
            @Override
            public void run() {
                long i = 0;
                while (true) {
                    try {
                        Thread.sleep(0, 1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    i++;
                }
            }
        });


    }
}
